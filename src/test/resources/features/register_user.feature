# Autor: Manuel Buelvas

  @Stories
  Feature: User registration on the Utest page

    @scenario1
    Scenario: Manuel registration on the Utest page
      Given Manuel wants to register on the Utest page
      When enter all required information
        | strFirsName | strLastName | strEmail          | strBirthMonth | strBirthDay | strBirthYeard | strCity | strPostalCode | strCountry | strComputer | strVersionComputer | strLenguageComputer | strMobileDevice | strDiviceModel | strOperatingSystem | strPassword |
        | Manuel      | Buelvas     |buelvasmb@gmail.com| March         | 6           |1996           | Medellín  | 050030      | Colombia   | Windows     | 10                 | Spanish             | Apple           | iPhone 11      | iOS 13             | Choucair2021*|
      Then registration is completed successfully